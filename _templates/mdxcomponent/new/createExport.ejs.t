---
to: src/MDXComponents/index.ts
inject: true
skip_if: <%= name %>
append: true
eof_last: false
---
export { default as <%= name %> } from './<%= name %>/<%= name %>';
