---
to: src/MDXComponents/<%= name %>/<%= name %>.stories.tsx
---
import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import <%= name %>, { <%= name %>Props } from './<%= name %>';

export default {
  title: '<%= name %>',
  component: <%= name %>,
} as Meta;

const Template: Story<<%= name %>Props> = (args) => (
  <<%= name %> {...args} />
)

export const <%= name %>Story = Template.bind({});

<%= name %>Story.args = {};
