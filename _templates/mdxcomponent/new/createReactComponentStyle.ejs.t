---
to: src/MDXComponents/<%= name %>/styles.ts
---
import styled from 'styled-components';

export const <%= name %>Component = styled.div``;
