---
to: src/MDXComponents/<%= name %>/<%= name %>.tsx
---
import React from 'react';
import * as S from './styles';

export interface <%= name %>Props {

}

const <%= name %>: React.FC<<%= name %>Props> = () => {
  return (
    <>
    </>
  )
}

export default <%= name %>
