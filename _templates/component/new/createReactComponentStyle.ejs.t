---
to: src/components/<%= name %>/styles.ts
---
import styled from 'styled-components';

export const <%= name %> = styled.div``;
