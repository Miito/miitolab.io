---
to: src/components/<%= name %>/<%= name %>.tsx
---
import React from 'react';
import * as S from './styles';

export interface <%= name %>Props {
  any: any;
}

const <%= name %>: React.FC<<%= name %>Props> = () => {
  return (
    <S.<%= name %>>
    </S.<%= name %>>
  );
};

export default <%= name %>;
