---
to: src/content/notes/<%= url %>/index.mdx
---
---
title: <%= title %>
description: <%= description %>
date: <%= Date.now(); %>
icon: <%= icon %>
type: article
ogImage: ''
tags:
  - react
---
