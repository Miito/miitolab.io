---
to: src/content/posts/<%= url %>/index.mdx
---
---
title: <%= title %>
description: <%= description %>
date: <%= Date.now(); %>
icon: <%= icon %>
type: article
ogImage: ''
tags:
  - react
---
