/* eslint-disable @typescript-eslint/no-var-requires */
const { createFilePath } = require('gatsby-source-filesystem');
const path = require('path');

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
      alias: {
        '@': path.resolve(__dirname, 'src')
      }
    }
  });
};

exports.onCreateNode = (args) => {
  const { node, actions, getNode } = args;
  const { createNodeField } = actions;

  if (node.internal.type === 'Mdx') {
    const value = createFilePath({ node, getNode });
    createNodeField({
      name: 'slug',
      node,
      value,
    });
  }
};

const createPagesForMdxForDirectory = async ({ directory, graphql, reporter, actions }) => {
  const { createPage } = actions;

  const result = await graphql(`
    query {
      allMdx(filter: {fileAbsolutePath: {glob: "**/${directory}/**"}}) {
        edges {
          node {
            id
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  if (result.error) {
    reporter.panicOnBuild('ERROR: Unable to load createPages query');
  }

  const mdx = result.data.allMdx.edges;
  const postsPerPage = 5;
  const numPages = Math.ceil(mdx.length / postsPerPage);

  if (directory === 'posts') {
  Array.from({ length: numPages }).forEach((_, i) => {
    createPage({
      path: i === 0 ? '/posts' : `/posts/${i + 1}`,
      component: path.resolve('./src/components/Renderer/Post/PostRenderer.tsx'),
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages,
        currentPage: i + 1,
        },
      });
    });
  }
  mdx.forEach(({ node }) => {
    createPage({
      path: `${directory}${node.fields.slug}`,
      component: path.resolve(`src/layouts/${directory}.tsx`),
      context: { id: node.id }
    });
  });
};

exports.createPages = async (args) => {
  const mdxSourceDirectories = [
    'download',
    'drafts',
    'notes',
    'portfolios',
    'posts'
  ];

  const promises = mdxSourceDirectories.map((directory) => {
    return createPagesForMdxForDirectory({ directory, ...args });
  });

  await Promise.all(promises);
};
