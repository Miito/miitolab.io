import { css } from 'styled-components';

export const noLinkStyle = css`
  background: none;
  color: transparent;

  &::before {
    display: none;
  }
`;
