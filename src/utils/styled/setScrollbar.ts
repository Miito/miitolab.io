import { css, FlattenSimpleInterpolation } from 'styled-components';
/* stylelint-disable no-descending-specificity */

export const setScrollbar = (): FlattenSimpleInterpolation => css`
  scrollbar-color: hsla(0, 0%, 100%, 0.5) hsla(0, 0%, 100%, 0.025);
  scrollbar-width: thin;

  ::-webkit-scrollbar {
    width: 10px;
    background-color: hsla(0, 0%, 100%, 0.025);
    border-radius: 100px;
  }

  ::-webkit-scrollbar:hover {
    background-color: rgba(0, 0, 0, 0.09);
  }

  ::-webkit-scrollbar:horizontal {
    height: 10px;
  }

  ::-webkit-scrollbar-thumb {
    background: hsla(0, 0%, 100%, 0.5);
    border-radius: 100px;
    background-clip: padding-box;
    border: 2px solid hsla(0, 0%, 100%, 0);
    min-height: 10px;
  }

  ::-webkit-scrollbar-thumb:active {
    background: hsla(0, 0%, 100%, 0.6);
    border-radius: 100px;
  }
`;
