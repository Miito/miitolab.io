import { useState, useEffect } from 'react';

type MousePosition = {
  x: number;
  y: number;
};

const useMousePosition = (): MousePosition => {
  const [mousePosition, setMousePosition] = useState({
    x: typeof window !== 'undefined' ? window.innerWidth / 2 : undefined,
    y: typeof window !== 'undefined' ? window.innerHeight / 2 : undefined
  });

  const updateMousePosition = ev => {
    setMousePosition({ x: ev.clientX, y: ev.clientY });
  };

  useEffect(() => {
    window.addEventListener('mousemove', updateMousePosition);

    return () => window.removeEventListener('mousemove', updateMousePosition);
  }, []);

  return mousePosition;
};

export default useMousePosition;
