import { useStaticQuery, graphql } from 'gatsby';

const getSiteName = (): GatsbyTypes.getSiteNameQuery => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const data = useStaticQuery<GatsbyTypes.getSiteNamQuery>(graphql`
    query getSiteName{
      site {
        siteMetadata {
          title
          author
        }
      }
    }
  `);

  return data;
};

export default getSiteName;

