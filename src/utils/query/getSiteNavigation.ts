import { graphql, useStaticQuery } from 'gatsby';

const getSiteNavigation = (): GatsbyTypes.getSiteNavigationQuery => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const data = useStaticQuery<GatsbyTypes.getSiteNavigationQuery>(graphql`
      query getSiteNavigation {
        site {
          siteMetadata {
            navLinks {
              name
              link
            }
          }
        }
      }
    `,
  );

  return data;
};

export default getSiteNavigation;
