import styled from 'styled-components';
import ReactPlayer from 'react-player/lazy';

export const MaxWidth = styled.div`
  margin-bottom: 40px;
  width: 100%;
  height: 100%;
`;

export const VideoContainer = styled.div`
  position: relative;
  padding-top: 56.25%; /* Player ratio: 100 / (1280 / 720) */
`;

export const Player = styled(ReactPlayer)`
  position: absolute;
  top: 0;
  left: 0;
  border-radius: 5px;
  overflow: hidden;
`;
