import React from 'react';
import * as S from './styles';

export interface VideoPlayerProps {
  url: string,
  props?: unknown,
}

const VideoPlayer: React.FC<VideoPlayerProps> = ({url, props}) => {
  return (
    <S.MaxWidth>
      <S.VideoContainer>
        <S.Player
          url={url}
          width='100%'
          height='100%'
          controls={ true }
          {...props}
        />
      </S.VideoContainer>
    </S.MaxWidth>
  );
};

export default VideoPlayer;
