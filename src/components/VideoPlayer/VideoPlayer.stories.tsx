import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import VideoPlayer, { VideoPlayerProps } from './VideoPlayer';

export default {
  title: 'VideoPlayer',
  component: VideoPlayer,
} as Meta;

const Template: Story<VideoPlayerProps> = (args) => (
  <VideoPlayer {...args} />
);

export const VideoPlayerStory = Template.bind({});

VideoPlayerStory.args = {};
