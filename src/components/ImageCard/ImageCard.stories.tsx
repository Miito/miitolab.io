import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import ImageCard, { ImageCardProps } from './ImageCard';

export default {
  title: 'ImageCard',
  component: ImageCard,
} as Meta;

const Template: Story<ImageCardProps> = (args) => (
  <ImageCard {...args} />
);

export const ImageCardStory = Template.bind({});

ImageCardStory.args = {};
