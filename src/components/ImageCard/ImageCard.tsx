import React from 'react';
import { TagRenderer } from '../Renderer/Tag/TagRenderer';
import * as S from './styles';
import { getImage } from 'gatsby-plugin-image';

export interface ImageCardProps {
  slug: string;
  frontmatter: string[];
  img: string;
  to: string;
  title?: string;
  description?: string;
}
const ImageCard: React.FC<ImageCardProps> = ({ data, slug, frontmatter, id }) => {
  const image = getImage(frontmatter.image);

  return (
    <S.ImageCard>
      <S.Image alt={frontmatter.imageAlt} image={image}/>
      <S.ImageOverlay />
      <S.HeaderLink to={slug}>
        {frontmatter.title && <S.Header>{frontmatter.title}</S.Header>}
        {frontmatter.description && <S.Description>{frontmatter.description}</S.Description>}
      </S.HeaderLink>
      { frontmatter.tags &&
        <S.ExtraInfo>
          <TagRenderer tags={frontmatter.tags}/>
        </S.ExtraInfo>
      }
    </S.ImageCard>
  );
};

export default ImageCard;
