import { ThemeType } from '@/styles/theme';
import { noLinkStyle } from '@/utils/styled/noLinkStyle';
import { Link } from 'gatsby';
import styled from 'styled-components';
import { GatsbyImage } from 'gatsby-plugin-image';

export const ImageCard = styled.div`
  border-radius: 6px;
  overflow: hidden;
  height: 227px;
  width: 100%;
  position: relative;
  display: grid;
  grid-template-columns: auto auto;
  grid-template-rows: auto auto auto;
`;

export const Image = styled(GatsbyImage)`
  &&& {
    position: initial;
  }

  ::selection {
    background-color: rgba(95, 25, 221, 0.5);
  }
`;

export const ImageOverlay = styled.div`
  height: 100%;
  width: 100%;
  position: absolute;
  left: 0;
  top: 0;
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.53) 16.15%, rgba(255, 255, 255, 0.27) 100%), linear-gradient(180deg, rgba(59, 0, 168, 0.2) 0%, rgba(95, 25, 221, 0.2) 100%), url(.jpg);
`;

export const HeaderLink = styled(Link)`
  ${noLinkStyle}
  position: absolute;
  bottom: 15px;
  left: 17px;
  color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_1};
  padding: 0;

  h2,
  h3 {
    padding: 0;
    margin: 0;
  }
`;

export const ExtraInfo = styled.div`
  position: absolute;
  background-color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_2};
  color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_2};
  fill: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_2};
  border-radius: 50px;
  top: 15px;
  right: 17px;
  height: 25px;
  min-width: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 5px;

  svg {
    height: 16px;
    padding: 0 4px;
  }
`;

export const Header = styled.h2`
  font-weight: 500;
  font-size: 14px;
`;

export const Description = styled.h3`
  font-weight: 700;
  font-size: 14px;
`;
