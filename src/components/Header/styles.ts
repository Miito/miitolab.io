import styled from 'styled-components';
import { Link } from 'gatsby';
import { Breakpoints } from 'styles/breakpoint';
import { FontWeight } from 'styles/typography';
import { MaxWidth } from 'components/MaxWidth/styles';
import { ThemeType } from 'styles/theme';
import { noLinkStyle } from '@/utils/styled/noLinkStyle';
import { HeaderSkipToContent } from '@/styles/z-index';

export const Background = styled.div`
  width: auto;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 25px;
  border-radius: 25px;
  flex-shrink: 0;

  @media (max-width: ${Breakpoints.medium}) {
    margin: 0 6vw;
  }
`;

export const LimitWidth = styled(MaxWidth)`
  padding: 0 55px;

  @media (max-width: ${Breakpoints.medium}) {
    padding: 0;
  }
`;

export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: ${Breakpoints.medium}) {
    padding: 0;
  }
`;

export const SkipToContent = styled.a`
  ${noLinkStyle}
  display: block;
  color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_1};
  background: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_2};
  margin: 6px;
  padding: 10px 15px;
  font-weight: 700;
  position: absolute;
  left: -500px;
  z-index: ${HeaderSkipToContent};

  &:focus {
    box-shadow: 0 0 0 2px ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_2};
  }

  &:link,
  &:visited {
    color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_2};
    text-decoration: none;
  }

  &:active,
  &:focus,
  &:hover {
    display: block;
    top: 0;
    left: 0;
  }

  @media print {
    display: none;
  }
`;

export const MainNav = styled.div`
  display: flex;
  align-items: center;
`;
export const Wordmark = styled(Link)`
  ${noLinkStyle}
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 12px;
  margin-left: -12px;
  color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_2};
  font-weight: ${FontWeight.semibold};
  font-family: ${(provider: { theme: ThemeType }) => provider.theme.types.wordmarkFontFamily};
  font-size: 16px;
  letter-spacing: 0.5px;

  &:hover {
    color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_1};
  }
`;
