import React from 'react';
import * as S from './styles';
import { Nav } from 'components';
import getSiteName from '@/utils/query/getSiteName';

const Header: React.FC = () => {
  const data = getSiteName();
  const siteName = data.site.siteMetadata.title;

  return (
    <S.Background>
      <S.LimitWidth>
        <S.Header role="banner">
          <S.SkipToContent href="#Skip">
            Skip to content
          </S.SkipToContent>
          <S.Wordmark to='/'>
            { siteName }
          </S.Wordmark>
          <Nav/>
        </S.Header>
      </S.LimitWidth>
    </S.Background>
  );
};

export default Header;
