import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import Header, { HeaderProps } from './Header';
import {
  createMemorySource,
  createHistory,
  LocationProvider
} from '@reach/router';

const source = createMemorySource('/');
const history = createHistory(source);

export default {
  title: 'Header',
  component: Header,
} as Meta;

const Template: Story<HeaderProps> = (args) => (
  <LocationProvider history={history}>
    <Header {...args} />
  </LocationProvider>
);

export const HeaderStory = Template.bind({});

HeaderStory.args = {};
