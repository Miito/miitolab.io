import React from 'react';
import { HeroBg, MaxWidth, Title } from 'components';
import * as S from './styles';
import { Link } from 'gatsby';

const Hero: React.FC = () => {
  return (
    <S.Hero>
      <MaxWidth>
        <S.HeroInner>
          <S.Row>
            <S.Col>
              <Title>Hey I'm Mito. I'm a web developer based in Ottawa.</Title>
              <S.AboutButton as={Link} to="/about">about me!</S.AboutButton>
            </S.Col>
            <S.ColBG>
              <HeroBg />
            </S.ColBG>
          </S.Row>
        </S.HeroInner>
      </MaxWidth>
    </S.Hero>
  );
};

export default Hero;
