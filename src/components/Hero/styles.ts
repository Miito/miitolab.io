import { noLinkStyle } from '@/utils/styled/noLinkStyle';
import styled from 'styled-components';
import { Button } from '../Button/styles';

export const Hero = styled.section`
  position: relative;
  display: flex;
  justify-content: center;
`;

export const HeroInner = styled.div`
  padding-bottom: 128px;
  padding-top: 116px;
`;

export const Row = styled.div`
  grid-template-columns: repeat(2, 1fr);
  display: grid;
  row-gap: 32px;
  align-items: flex-start;
`;

export const Col = styled.div`
  z-index: 1;
`;

export const ColBG = styled(Col)`
  z-index: 0;
  opacity: 0.5;
`;

export const AboutButton = styled(Button)`
  ${noLinkStyle}
  background-color: ${(provider: { theme: ThemeType }) => provider.theme.colors.mito_purple_2};
  color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_1};
`;
