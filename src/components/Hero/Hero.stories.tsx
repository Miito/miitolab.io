import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import Hero, { HeroProps } from './Hero';

export default {
  title: 'Hero',
  component: Hero,
} as Meta;

const Template: Story<HeroProps> = (args) => (
  <Hero {...args} />
);

export const HeroStory = Template.bind({});

HeroStory.args = {};
