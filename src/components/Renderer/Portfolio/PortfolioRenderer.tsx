import * as React from 'react';
import MainLayout from 'layouts/Main';
import { MaxWidth, SEO, Title, ImageCard } from 'components';
import * as S from './styles';

interface PortfolioProps {
  data: GatsbyTypes.PortfoliosIndexQuery;
}

const PortfolioRenderer: React.FC<PortfolioProps> = ({ data }) => {
  const { edges: articles } = data.allMdx;

  return (
    <MainLayout>
      <SEO title="portfolio"/>
      <MaxWidth>
        <Title>Portfolio</Title>
        <S.Row>
          {articles.map(({ node }) => {
            return (
              <ImageCard key={node.id} {...node}/>
            );
          })}
        </S.Row>
      </MaxWidth>
    </MainLayout>
  );
};

export default PortfolioRenderer;
