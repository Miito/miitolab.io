import React, { useEffect, useState } from 'react';
import MainLayout from 'layouts/Main';
import { MaxWidth, SEO, Title, DownloadCard, DownloadPage } from 'components';
import * as S from './styles';

interface DownloadProps {
  data: GatsbyTypes.DownloadsIndexQuery;
}

const DownloadRenderer: React.FC<DownloadProps> = ({ data }) => {
  const { edges: articles } = data.allMdx;
  const [selected, setSelected] = useState();
  const [page, setPage] = useState();

  const onSelect = (id) => {
    setSelected(id);
  };

  useEffect(() => {
    if (selected) {
      articles.forEach((page) => {
        if (page.node.id === selected) {
          setPage(page);
        }
      });
    }
  }, [articles, selected]);

  return (
    <MainLayout>
      <SEO title="download"/>
      <MaxWidth>
        <Title>Download</Title>
        <S.Row>
          <S.Col33>
            <S.FileTitle>File List</S.FileTitle>
            <S.FileSelector>
              {articles.map(({ node }) =>
                <DownloadCard key={node.id} selected={node.id === selected} onClick={() => onSelect(node.id)} {...node}/>
              )}
            </S.FileSelector>
          </S.Col33>
          <S.Col66>
            <DownloadPage page={page}/>
          </S.Col66>
        </S.Row>
      </MaxWidth>
    </MainLayout>
  );
};

export default DownloadRenderer;
