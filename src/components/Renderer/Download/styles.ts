import { Breakpoints } from '@/styles/breakpoint';
import { ThemeType } from '@/styles/theme';
import { setScrollbar } from '@/utils/styled/setScrollbar';
import styled, { css } from 'styled-components';

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 10px;

  @media (max-width: ${Breakpoints.medium}) {
    flex-direction: column;
  }
`;

const StyleCol = css`
  background: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_2};
  border-radius: 6px;
  height: 100%;
  width: 100%;
  ${setScrollbar}
`;

export const Col33 = styled.div`
  flex-basis: calc((100% / 3) * 1 - 15px);
  ${StyleCol}
`;

export const Col66 = styled.div`
  flex-basis: calc((100% / 3) * 2 - 15px);
  ${StyleCol}
`;

export const FileTitle = styled.h2`
  margin: 15px 17px;
`;

export const FileSelector = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  overflow-x: auto;
  ${setScrollbar}

  @media (max-width: ${Breakpoints.medium}) {
    flex-direction: row;
  }
`;
