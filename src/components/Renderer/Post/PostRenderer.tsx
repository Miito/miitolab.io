import React from 'react';
import * as S from './styles';
import { MaxWidth, Post, SEO, Title } from 'components';
import MainLayout from 'layouts/Main';
import { graphql, Link } from 'gatsby';

interface PostProps {
  data: unknown;
  pageContext: {
    currentPage: number;
    numPages: number;
  };
}

const PostRenderer: React.FC<PostProps> = ({ data, pageContext }) => {
  const { edges } = data.allMdx;
  const { currentPage, numPages } = pageContext;
  const isFirst = currentPage === 1;
  const isLast = currentPage === numPages;
  const prevPage = currentPage - 1 === 1 ? '' : (currentPage - 1).toString();
  const nextPage = (currentPage + 1).toString();
  // const [search, setSearch] = useState("");
  // const handleSearch = (e) => {
  //   setSearch(e.target.value);
  // };

  return (
    <MainLayout>
      <SEO title="posts"/>
      <S.Blog>
        <MaxWidth>
          <S.ArticlesComponent>
            <S.Header>
              <Title>Post</Title>
              {/* <Search onChange={(e) => handleSearch(e)} /> */}
            </S.Header>
            <ul>
              {edges.map(({ node }, i) =>
                <S.Li key={node.id}>
                  <Post
                    slug={`/posts/${node.slug}`}
                    frontmatter={node.frontmatter}
                    body = {node.body}
                    header="h2"
                    disableSEO={true}
                  />
                  { i === edges.length - 1 ? <></> : <S.Divider />}
                </S.Li>
              )}
            </ul>
            <S.Pagination>
              {!isFirst && (
                <Link to={`/posts/${prevPage}`} rel="prev">
                  ← Previous Page
                </Link>
              )}
              {Array.from({ length: numPages }, (_, i) => (
                <Link key={`pagination-number${i + 1}`} to={`/${i === 0 ? 'posts/' : `posts/${i + 1}`}`}>
                  {i + 1}
                </Link>
              ))}
              {!isLast && (
                <Link to={`/posts/${nextPage}`} rel="next">
                  Next Page →
                </Link>
              )}
            </S.Pagination>
          </S.ArticlesComponent>
        </MaxWidth>
      </S.Blog>
    </MainLayout>
  );
};

export default PostRenderer;

export const pageQuery = graphql`
query PostsIndex($skip: Int!, $limit: Int!) {
  allMdx(
    filter: {
      fileAbsolutePath: {glob: "**/posts/**"}},
      sort: {fields: [frontmatter___date],
      order: DESC
    },
    limit: $limit
    skip: $skip
    ) {
      edges {
        node {
          id
          excerpt
          slug
          body
          frontmatter {
            title
            date
            tags
            description
          }
        }
      }
  }
}
`;
