import { ThemeType } from '@/styles/theme';
import styled from 'styled-components';
import { Breakpoints } from 'styles/breakpoint';

export const Blog = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

export const ArticlesComponent = styled.div`
  ul {
    margin: 0;
    padding: 0;
  }
`;

export const Header = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Li = styled.li`
  list-style-type: none;
  margin-bottom: 34px;
  padding-bottom: 34px;

  &:last-child {
    border: 0;
  }

  @media (min-width: ${Breakpoints.medium}) {
    margin-bottom: 60px;
  }
`;

export const Divider = styled.hr`
  @media (min-width: ${Breakpoints.medium}) {
    margin-bottom: 40px;
  }
`;

export const Title = styled.h1`
  margin-top: 25px;
  margin-bottom: 50px;
  color: ${(props: { theme: ThemeType }) => props.theme.colors.purple_text_1};

  @media (max-width: ${Breakpoints.medium}) {
    margin: 30px 0 34px 0;
  }
`;

export const Pagination = styled.div`
  display: flex;
  justify-content: center;

  a {
    margin: 0 3px;
    padding: 0 4px;
    min-width: 25px;
    text-align: center;
  }
`;
