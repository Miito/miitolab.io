import slugify from '@/utils/slugify';
import React from 'react';
import { TagList } from './TagList';

interface TagProps {
  tags: string[];
}

export const TagRenderer: React.FC<TagProps> = ({ tags }) => {
  const renderList = [];

  if (!tags) {
    return;
  }

  TagList.forEach((Icon) => {
    tags.forEach((tag) => {
      if (renderList.includes(Icon)) {
        return;
      }

      if (Icon.name.toLowerCase() === tag.toLowerCase()) {
        renderList.push(Icon);
      }
    });
  });

  return (
    <>
      {Object.values(renderList).map((item)  => (
        <item.icon key={slugify(item.name)}/>
      ))}
    </>
  );
};
