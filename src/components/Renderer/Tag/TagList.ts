import IconList, { NodeJS, Express, HTML5, JavaScript, jQuery, MySql, Passport, Sequelize } from '@/assets/icons';

export const TagList = [
  {
    name: 'React',
    icon: IconList.ReactIcon,
  },
  {
    name: 'Code',
    icon: IconList.Code,
  },
  {
    name: 'Express',
    icon: Express,
  },
  {
    name: 'HTML5',
    icon: HTML5,
  },
  {
    name: 'JavaScript',
    icon: JavaScript,
  },
  {
    name: 'jQuery',
    icon: jQuery,
  },
  {
    name: 'MySql',
    icon: MySql,
  },
  {
    name: 'NodeJS',
    icon: NodeJS,
  },
  {
    name: 'Passport',
    icon: Passport,
  },
  {
    name: 'Sequelize',
    icon: Sequelize,
  },
];
