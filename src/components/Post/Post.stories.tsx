import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import Post, { PostProps } from './Post';

export default {
  title: 'Post',
  component: Post,
} as Meta;

const Template: Story<PostProps> = (args) => (
  <Post {...args} />
);

export const PostStory = Template.bind({});

PostStory.args = {};
