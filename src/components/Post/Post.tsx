import React from 'react';
import dayjs from 'dayjs';
import { MDXProvider } from '@mdx-js/react';
import { MDXRenderer } from 'gatsby-plugin-mdx';

import * as S from './styles';
import Code from '../CodeBlock/Code';
import { SEO } from '..';

const components = {
  p: props => <S.P {...props} />,
  li: props => <S.Li {...props} />,
  // eslint-disable-next-line jsx-a11y/heading-has-content
  h2: props => <h2 {...props} />,
  code: props => <Code {...props} />,
};

interface PostProps {
  slug?: string;
  frontmatter?: unknown;
  body?: unknown;
  header?: unknown;
  disableSEO?: boolean;
}

const Post: React.FC<PostProps> = ({ slug, frontmatter, body, header, disableSEO }) => {
  return (
    <S.Post>
      {!disableSEO && <SEO title={frontmatter.title} />}
      <S.StyledLink to={slug}>
        <S.Title as={header}>{frontmatter.title}</S.Title>
      </S.StyledLink>
      <S.Date datetime={frontmatter.date}>
        published {dayjs.unix(frontmatter.date).format('MMMM DD, YYYY')}
      </S.Date>
      <S.Description>
        <MDXProvider components={components}>
          <MDXRenderer>
            {body}
          </MDXRenderer>
        </MDXProvider>
      </S.Description>
    </S.Post>
  );
};


export default Post;
