import { noLinkStyle } from 'utils/styled/noLinkStyle';
import styled from 'styled-components';
import { Link } from 'gatsby';
import { ThemeType } from '@/styles/theme';

export const Post = styled.div``;

export const StyledLink = styled(Link)`
  ${noLinkStyle}
  display: inline-block;
  padding: 0;
`;

export const Title = styled.h1`
  font-size: 20px;
  margin-bottom: 7px;
  margin-top: 8px;
  color: ${(props: { theme: ThemeType }) => props.theme.colors.purple_text_1};

  &:hover {
    text-decoration: underline;
  }
`;

export const Date = styled.time`
  font-size: 12px;
  color: ${(props: { theme: ThemeType }) => props.theme.colors.purple_text_1};
  margin-top: 3px;
  display: block;
`;

export const Description = styled.div`
  margin: 20px 0 0 0;
`;

export const ReadMore = styled.a`
  color: ${(props: { theme: ThemeType }) => props.theme.colors.purple_text_1};
  margin-top: 25px;
  display: block;
`;

export const P = styled.p`
  font-size: 15px;
  margin-bottom: 24px;
`;

export const Ul = styled.ul``;

export const Li = styled.li`
  font-size: 16px;
  margin-bottom: 1rem;
  list-style-type: none;
`;
