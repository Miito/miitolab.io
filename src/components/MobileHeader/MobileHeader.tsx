import React from 'react';
import { SkipToContent } from '../Header/styles';
import { Nav } from 'components';
import * as S from './styles';

export interface MobileHeaderProps {
  any: any;
}

const MobileHeader: React.FC<MobileHeaderProps> = () => {
  return (
    <S.MobileHeader>
      <S.Header role="banner">
        <SkipToContent href="#Skip">
          Skip to content
        </SkipToContent>
        <Nav />
      </S.Header>
    </S.MobileHeader>
  );
};

export default MobileHeader;
