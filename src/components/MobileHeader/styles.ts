import styled from 'styled-components';

export const MobileHeader = styled.div`
  position: fixed;
  width: 100vw;
  bottom: 0;

  justify-content: center;
`;

export const Header = styled.header``;
