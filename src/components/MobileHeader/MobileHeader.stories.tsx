import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import MobileHeader, { MobileHeaderProps } from './MobileHeader';

export default {
  title: 'MobileHeader',
  component: MobileHeader,
} as Meta;

const Template: Story<MobileHeaderProps> = (args) => (
  <MobileHeader {...args} />
)

export const MobileHeaderStory = Template.bind({});

MobileHeaderStory.args = {};
