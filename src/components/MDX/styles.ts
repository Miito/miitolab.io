import styled, { css } from 'styled-components';
import { Link } from 'gatsby';
import { Breakpoints } from 'styles/breakpoint';

const articleMaxWidth = css`
  width: 100vw;
  max-width: 810px;
  box-sizing: border-box;

  @media (max-width: ${Breakpoints.medium}) {
    max-width: 100vw;
  }
`;

export const A = styled.a`
  text-decoration: underline;
  color: #fff;
  text-decoration-color: #fff;
`;

export const StyledLink = styled(Link)`
  color: #fff;
`;

export const Ul = styled.ul`
  ${articleMaxWidth};
  width: 100%;
  margin-top: 0;
  padding-left: 130px;

  @media (max-width: ${Breakpoints.medium}) {
    padding: 0 6vw 0 17vw;
  }
`;

export const Ol = styled.ol`
  ${articleMaxWidth};
  width: 100%;
  margin-top: 0;
  padding-left: 130px;

  @media (max-width: ${Breakpoints.medium}) {
    padding: 0 6vw 0 17vw;
  }
`;

export const P = styled.p`
  ${articleMaxWidth};
  width: 100%;
  font-size: 16px;
  margin-bottom: 2rem;
  color: #fff;

  @media (min-width: ${Breakpoints.medium}) {
    font-size: 18px;
    line-height: 28px;
  }

  code {
    font-size: 15px;
    background-color: #fff;
    color: #000;
    padding: 3px 6px;
    margin: 0 2px;
    border-radius: 2px;
  }
`;

export const Img = styled.img`
  max-width: 100%;
  display: block;
  & {
    box-shadow: none;
  }
`;

export const Pre = styled.pre`
  margin-top: 0;
`;

export const Li = styled.li`
  font-size: 16px;
  margin-bottom: 1rem;
  color: #fff;

  @media (min-width: ${Breakpoints.medium}) {
    font-size: 18px;
  }

  p,
  ${Pre} {
    padding: 0;
  }
`;

export const H1 = styled.h1`
  color: #fff;
`;

export const H2 = styled.h2`
  margin: 3rem 0 1rem 0;

  @media (min-width: ${Breakpoints.medium}) {
    margin: 4rem 0 2rem 0;
  }
`;

export const H3 = styled.h3`
  margin: 2rem 0 0.5rem 0;

  @media (min-width: ${Breakpoints.medium}) {
    margin: 2rem 0 1rem 0;
  }
`;

export const Hr = styled.hr`
  background-color: #fff;
  border: 0;
  height: 1px;
  margin: 60px 0;
`;

export const Summary = styled.summary`
  margin-bottom: 20px;
  padding-top: 6px;
  padding-bottom: 6px;
  cursor: pointer;
  transition: all 200ms ease;
  font-weight: #fff;

  &:hover {
    color: #fff;
  }

  &:focus {
    outline: none;
    box-shadow: 0 0 0 2px #fff;
    border-radius: 4px;
  }
`;
