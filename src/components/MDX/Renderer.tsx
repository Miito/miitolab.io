import React from 'react';
import * as S from './styles';
import { Code } from 'components';
import { useLocation } from '@reach/router';
import slugify from 'utils/slugify';

const Header = Tag => props => {
  const El = S[Tag];
  const slug = slugify(props.children.toString()).toLowerCase();
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const location = useLocation();

  return (
    <El {...props} id={slug}>
      <S.StyledLink to={`${location.pathname}#${slug}`}>
        {props.children}
      </S.StyledLink>
    </El>
  );
};

const MDXRenderer = {
  /* eslint-disable @typescript-eslint/explicit-module-boundary-types */
  a: (props) => <S.A {...props} />,
  code: (props) => <Code {...props} />,
  h1: S.H1,
  h2: Header('H2'),
  h3: Header('H3'),
  hr: S.Hr,
  img: S.Img,
  li: props => <S.Li {...props} />,
  ol: props => <S.Ol {...props} />,
  p: S.P,
  pre: S.Pre,
  summary: S.Summary,
  ul: props => <S.Ul {...props} />,
  /* eslint-enable @typescript-eslint/explicit-module-boundary-types */
};
export default MDXRenderer;
