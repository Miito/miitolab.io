import { ThemeType } from '@/styles/theme';
import styled, { css } from 'styled-components';
import { GatsbyImage } from 'gatsby-plugin-image';


export const DownloadCard = styled.div`
  border-radius: 6px;
  width: 100%;
  height: 100px;
  min-width: 255px;
  position: relative;
  overflow: hidden;
  cursor: pointer;

  ${(props) => props.selected && css`
  border: 5px solid ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_4};
  `}
`;

export const Image = styled(GatsbyImage)`
  & {
    position: initial;
  }

  ::selection {
    background-color: rgba(95, 25, 221, 0.5);
  }
`;

export const ImageOverlay = styled.div`
  height: 100%;
  width: 100%;
  position: absolute;
  left: 0;
  top: 0;
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.53) 16.15%, rgba(255, 255, 255, 0.27) 100%), linear-gradient(180deg, rgba(59, 0, 168, 0.2) 0%, rgba(95, 25, 221, 0.2) 100%);
`;

export const Info = styled.div`
  position: absolute;
  left: 17px;
  bottom: 15px;
`;

const StyleText = css`
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  margin: 0;
`;

export const Header = styled.h2`
  ${StyleText}
`;
export const Description = styled.h3`
  ${StyleText}
`;
