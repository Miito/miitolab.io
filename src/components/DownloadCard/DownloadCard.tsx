import { getImage } from 'gatsby-plugin-image';
import React from 'react';
import * as S from './styles';

export interface DownloadCardProps {
  frontmatter?: unknown[];
  onClick?: React.MouseEventHandler<HTMLDivElement>;
  selected?: boolean;
  props?: unknown;
}

const DownloadCard: React.FC<DownloadCardProps> = ({ frontmatter, onClick, selected, props }) => {
  const image = getImage(frontmatter.image);

  return (
    <S.DownloadCard onClick={onClick} selected={selected} {...props}>
      <S.Image alt={frontmatter.imageAlt} image={image}/>
      <S.ImageOverlay />
      <S.Info>
        <S.Header>{frontmatter.title}</S.Header>
        <S.Description>{frontmatter.description}</S.Description>
      </S.Info>
    </S.DownloadCard>
  );
};

export default DownloadCard;
