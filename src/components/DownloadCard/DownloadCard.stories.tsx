import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import DownloadCard, { DownloadCardProps } from './DownloadCard';

export default {
  title: 'DownloadCard',
  component: DownloadCard,
} as Meta;

const Template: Story<DownloadCardProps> = (args) => (
  <DownloadCard {...args} />
);

export const DownloadCardStory = Template.bind({});

DownloadCardStory.args = {};
