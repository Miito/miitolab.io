import React from 'react';
import * as S from './styles';
import getSiteNavigation from 'utils/query/getSiteNavigation';
import { useLocation } from '@reach/router';
import useWindowSize from 'utils/hook/useWindowSize';

export interface NavItemProps {
  selected?: boolean;
}

export interface NavLinkProps {
  selected?: boolean;
}

const Nav: React.FC = () => {
  const location = useLocation();
  const windowSize = useWindowSize();
  const data = getSiteNavigation();

  const isMobile = windowSize.width <= 650;
  const NavLinks = data.site.siteMetadata.navLinks;

  return (
    <S.Nav role="navigation" isMobile={isMobile}>
      {Object.values(NavLinks).map((item) => (
        <S.NavItem
          key={item.link}
          selected={item.link === location.pathname}
        >
          <S.NavLink
            to={item.link}
            selected={item.link === location.pathname}
          >
            {item.name}
          </S.NavLink>
        </S.NavItem>
      ))}
  </S.Nav>
  );
};

export default Nav;
