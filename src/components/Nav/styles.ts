import styled, { css } from 'styled-components';
import { Link } from 'gatsby';
import { Breakpoints } from 'styles/breakpoint';
import { NavItemProps, NavLinkProps } from './Nav';
import { ThemeType } from '@/styles/theme';
import { noLinkStyle } from '@/utils/styled/noLinkStyle';

export const Nav = styled.nav`
  position: relative;
  display: flex;

  ${(props) => props.isMobile && css`
    position: fixed;
    width: 100vw;
    bottom: 0;
    left: 0;
    justify-content: space-evenly;
    background: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_2};
    z-index: 2;
  `}
`;



export const NavItem = styled.div<NavItemProps>`
  display: flex;
  align-items: center;
  height: 60px;
  position: relative;

  &::after {
    content: '';
    display: block;
    width: 100%;
    height: 2px;
    position: absolute;
    bottom: 0;
    left: 0;
    transition: background 300ms ease;

    ${(props) => props.selected && css`
      background-color: ${(provider: { theme: ThemeType }) => provider.theme.colors.mito_purple_2};
    `}
  }

  a {
    ${(props) => props.selected && css`
      color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_1};
    `}
  }
`;

export const NavLink = styled(Link)<NavLinkProps>`
  ${noLinkStyle}
  align-items: center;
  box-sizing: border-box;
  color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_2};
  display: flex;
  font-size: 17px;
  font-weight: 500;
  padding: 12px;
  text-decoration: none;

  &:hover {
    color: ${(props) => props.color || props.theme.colors.purple_text_1};
    text-decoration: none;
  }

  &:focus {
    outline: none;
    box-shadow: 0 0 0 2px ${(props) => props.color || props.theme.colors.mito_purple_2};
    color: ${(props) => props.color || props.theme.colors.purple_text_1};
    border-radius: 4px;
    margin: 2px;
    padding: 10px;
  }

  @media (max-width: ${Breakpoints.medium}) {
    font-size: 14px;
    margin: 0 8px;
    padding: 5px;
  }
`;
