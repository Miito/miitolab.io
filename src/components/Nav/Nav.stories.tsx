import { Meta } from '@storybook/react/types-6-0';

import Nav from './Nav';

export default {
  title: 'Nav',
  component: Nav,
} as Meta;
