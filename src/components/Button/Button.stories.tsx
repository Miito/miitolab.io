import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import Button, { ButtonProps } from './Button';

export default {
  title: 'Button',
  component: Button,
} as Meta;

const Template: Story<ButtonProps> = (args) => (
  <Button {...args}>
    {args.children}
  </Button>
);

export const Default = Template.bind({});
Default.args = {
  children: 'Default',
};

export const Primary = Template.bind({});
Primary.args = {
  children: 'Primary',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  children: 'Small',
};
export const Medium = Template.bind({});
Medium.args = {
  size: 'medium',
  children: 'Medium',
};
export const Large = Template.bind({});
Large.args = {
  size: 'large',
  children: 'Large',
};
