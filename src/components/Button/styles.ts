import { ThemeType } from '@/styles/theme';
import styled from 'styled-components';
import { ButtonProps } from './Button';

export const Button = styled.button<ButtonProps>`
  padding: 6px 20px;
  color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_1};
  font-size: 16px;
  font-weight: 500;
  background-color: ${(provider: { theme: ThemeType }) => provider.theme.colors.mito_purple_2};
  border: 0;
  border-radius: 6px;
  appearance: none;
  cursor: pointer;

  &:hover {
    color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_1};
    background-color: ${(provider: { theme: ThemeType }) => provider.theme.colors.mito_purple_2};
  }

  &:disabled {
    color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_2};
    background-color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_2};
    opacity: 0.7;
    cursor: default;
  }
`;
