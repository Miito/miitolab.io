import { Meta } from '@storybook/react/types-6-0';

import MaxWidth from './MaxWidth';

export default {
  title: 'MaxWidth',
  component: MaxWidth,
} as Meta;
