import React, { useRef } from 'react';
import Button from '../Button/Button';
import * as S from './styles';
import { MDXProvider } from '@mdx-js/react';
import { MDXRenderer } from 'gatsby-plugin-mdx';
import Code from '../CodeBlock/Code';
import { Img } from '@/MDXComponents';
import { useTransform ,useElementScroll } from 'framer-motion';

const components = {
  p: props => <S.P {...props} />,
  li: props => <S.Li {...props} />,
  // eslint-disable-next-line jsx-a11y/heading-has-content
  h2: props => <S.h2 {...props} />,
  code: props => <Code {...props} />,
  img: props => <Img {...props} />
};

export interface DownloadPageProps {
  page: unknown;
  downloadLink: string;
}

const NoPage: React.FC = () => {
  return (
    <S.Center>
      <h2>Waiting...</h2>
    </S.Center>
  );
};

const HasPage: React.FC<DownloadPageProps> = ({ page, downloadLink }) => {
  const { title, description } = page.node.frontmatter;
  const scrollRef = useRef(null);
  const { scrollY } = useElementScroll(scrollRef);

  const padding = useTransform(
    scrollY,
    [0, 30],
    ['20px 28px', '10px 18px']
  );

  const borderBottom = useTransform(
    scrollY,
    [0, 30],
    ['2px solid rgba(0, 0, 0, 0)', '2px solid rgba(89, 71, 128, 1)']
  );


  return (
    <S.Download ref={scrollRef}>
      <S.PlaceHolder>
        <S.Info style={{
          padding: padding,
          borderBottom: borderBottom,
          }}>
          <S.Col>
            <S.H2>{title} {description}</S.H2>
          </S.Col>
          <S.ColFlexEnd>
            <Button>
              Download
            </Button>
          </S.ColFlexEnd>
        </S.Info>
      </S.PlaceHolder>
      <S.Divider />
      <S.Main>
        <MDXProvider components={components}>
          <MDXRenderer>
            {page.node.body}
          </MDXRenderer>
        </MDXProvider>
      </S.Main>
    </S.Download>
  );
};

const DownloadPage: React.FC<DownloadPageProps> = ({ page }) => {
  return (
    <>
      { page !== undefined ? <HasPage page={page}/> : <NoPage /> }
    </>
  );
};

export default DownloadPage;
