import { Breakpoints } from '@/styles/breakpoint';
import { ThemeType } from '@/styles/theme';
import { setScrollbar } from '@/utils/styled/setScrollbar';
import { motion } from 'framer-motion';
import styled from 'styled-components';

export const Download = styled.article`
  height: 100%;
  max-height: 500px;
  overflow-y: auto;
  border-radius: 6px;
  ${setScrollbar}
`;

export const DownloadPage = styled.div`
  background: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_2};
`;

export const Center = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const Info = styled(motion.header)`
  padding: 20px 28px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_2};
  border-bottom: 2px solid transparent;
  transition: all 50ms ease-in-out;

  h2,
  h3 {
    margin: 0;
  }
`;

export const PlaceHolder = styled.div`
  height: 72px;
  background: transparent;
  position: sticky;
  top: 0;
`;
export const Col = styled.div`
  display: inherit;
  align-items: center;
`;

export const ColFlexEnd = styled(Col)`
  align-items: flex-end;
`;

export const Divider = styled.hr`
  width: calc(100% - 56px);
  border: 1px solid ${(props: { theme: ThemeType }) => props.theme.colors.purple_4};
  border-radius: 6px;
  margin: 0 28px;
`;
export const Main = styled.main`
  margin: 0 28px;
`;

export const P = styled.p`
  font-size: 15px;
  margin-bottom: 24px;
`;

export const Ul = styled.ul``;

export const Li = styled.li`
  font-size: 16px;
  margin-bottom: 1rem;
  list-style-type: none;
`;

export const H2 = styled.h2`
  @media (max-width: ${Breakpoints.medium}) {
    font-size: 16px;
  }
`;
