import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import DownloadPage, { DownloadPageProps } from './DownloadPage';

export default {
  title: 'DownloadPage',
  component: DownloadPage,
} as Meta;

const Template: Story<DownloadPageProps> = (args) => (
  <DownloadPage {...args} />
);

export const DownloadPageStory = Template.bind({});

DownloadPageStory.args = {};
