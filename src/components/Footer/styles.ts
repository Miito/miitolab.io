import styled from 'styled-components';
import { Breakpoints } from 'styles/breakpoint';
import { MaxWidth } from '../MaxWidth/styles';
import { noLinkStyle } from 'utils/styled/noLinkStyle';
import { ThemeType } from '@/styles/theme';
export const Footer = styled.footer`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: auto;
  height: ${(provider: { theme: ThemeType }) => provider.theme.config.footer.height};
  flex-shrink: 0;

  @media (max-width: ${Breakpoints.medium}) {
    flex-direction: column;
    align-items: flex-start;
    padding: 55px 0;
  }
`;

export const CustomMaxWidth = styled(MaxWidth)`
  margin: 60px;
  padding: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: ${Breakpoints.medium}) {
    flex-direction: column;
    align-items: flex-start;
    padding-top: 0;
    padding-bottom: 0;
    width: auto;
  }
`;

export const Credits = styled.div`
  p {
    font-size: 16px;
    color: #fff;
    line-height: 180%;
  }

  a {
    text-decoration: none;
  }

  @media (max-width: ${Breakpoints.medium}) {
    margin-bottom: 40px;
  }
`;

export const SocialMedia = styled.div`
  display: flex;
  align-items: center;
`;
export const SocialLink = styled.a`
  margin-left: 20px;
  line-height: 0;
  ${noLinkStyle}

  @media (max-width: ${Breakpoints.medium}) {
    &:first-of-type {
      margin: 0;
    }
  }
`;
export const SocialIcon = styled.svg`
  height: 35px;
  width: 35px;
  padding: 5px;

  path {
    transition: fill 300ms ease;
  }

  &:hover path {
    fill: ${(props) => (props.color ? props.color : 'white')};
  }
`;
