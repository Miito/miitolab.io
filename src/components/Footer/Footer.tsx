import React from 'react';
import * as S from './styles';
import socialMedia from 'config/SocialMedia';

const Footer: React.FC = () => {
  return (
    <S.Footer role="contentinfo">
      <S.CustomMaxWidth>
        <S.Credits>
          <p>Designed and developed by Brian Dinh.</p>
          <p>Built with <a href='https://www.gatsbyjs.com/'>Gatsby</a>. Hosted on <a href="https://gitlab.com/">GitLab</a>.</p>
        </S.Credits>
        <S.SocialMedia>
          {Object.values(socialMedia).map((item)  => (
            <S.SocialLink key={item.url} href={item.url}>
              <S.SocialIcon key={item.url} fill={item.fill} color={item.hover}>
                <item.component />
              </S.SocialIcon>
            </S.SocialLink>
          ))}
        </S.SocialMedia>
      </S.CustomMaxWidth>
    </S.Footer>
  );
};

export default Footer;
