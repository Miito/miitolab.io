import React, { ChangeEventHandler } from 'react';
import * as S from './styles';
import { Search as SearchIcon } from '@/assets/icons/SearchIcon';

export interface SearchProps {
  onChange: ChangeEventHandler<HTMLInputElement>;
}

const Search: React.FC<SearchProps> = ({ onChange }) => {
  return (
    <S.Container>
      <S.Search onChange={ onChange } placeholder="Search"/>
      <S.SearchButton><SearchIcon /></S.SearchButton>
    </S.Container>
  );
};

export default Search;
