import { ThemeType } from '@/styles/theme';
import styled from 'styled-components';
import { Button } from '../Button/styles';

export const Container = styled.div`
  display: flex;
  align-items: center;
`;
export const Search = styled.input`
  background: ${(props: { theme: ThemeType }) => props.theme.colors.purple_2};
  border: none;
  border-radius: 6px 0 0 6px;
  color: ${(props: { theme: ThemeType }) => props.theme.colors.purple_text_1};
  padding: 8px 3px;
  height: 30px;

  ::placeholder {
    color: ${(props: { theme: ThemeType }) => props.theme.colors.purple_text_1};
  }
`;

export const SearchButton = styled(Button)`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  padding: 6px;
  border-radius: 0 6px 6px 0;

  svg {
    color: #fff;
    height: 18px;
    width: 18px;
  }
`;
