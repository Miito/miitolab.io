import { Breakpoints } from 'styles/breakpoint';
import styled from 'styled-components';
import { ThemeType } from '@/styles/theme';

export const Title = styled.h1`
  margin-top: 25px;
  margin-bottom: 50px;
  color: ${(provider: { theme: ThemeType }) => provider.theme.colors.purple_text_2};

  @media (max-width: ${Breakpoints.medium}) {
    margin: 30px 0 34px 0;
  }
`;
