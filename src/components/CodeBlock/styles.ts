import styled from 'styled-components';
import('./Theme.css');

export const Wrapper = styled.div`
  border-radius: 6px;
  line-height: 150%;
  padding: 0.9rem;
  margin-top: 0;
`;

export const Code = styled.code`
  display: inline-block;
  min-width: 100%;
  font-size: 15px;
  padding: 0;
  white-space: pre-wrap;
`;

export const Line = styled.div`
  display: table-row;
`;

export const LineNo = styled.span`
  display: table-cell;
  text-align: right;
  padding-right: 15px;
  user-select: none;
  color: #f2ebffab;
`;

export const LineContent = styled.span`
  display: table-cell;
`;
