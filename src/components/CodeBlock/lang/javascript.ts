import Prism from 'prism-react-renderer/prism';

(typeof global !== 'undefined' ? global : window).Prism = Prism;

Prism.languages.insertBefore('javascript', 'class-name', {
  'parent-class': {
    pattern: /\b(extends\s)[\w.\\]+\b/,
    lookbehind: true,
  }
});

Prism.languages.insertBefore('javascript', 'keyword', {
  'json-key': {
    pattern: /(\w+)(?=:)/i,
  },
  module: {
    pattern: /\b(?:import|as|export|from|default)\b/,
    alias: 'keyword',
  },
  op: {
    pattern: /\b(?:typeof|of|delete)\b/,
  },
  nil: {
    pattern: /\b(?:null|undefined)\b/,
    alias: 'keyword',
  },
  flow: {
    pattern: /\b(?:return|await)\b/,
    alias: 'keyword',
  },
  func: {
    pattern: /(\.\s*)[$_a-z][\w$]*(?=(\())/i,
    lookbehind: true,
  },
  declarations: {
    pattern: /\b(?:var|let|const)\b/,
  }
});

Prism.languages.insertBefore('javascript', 'punctuation', {
  definition: {
    pattern: /[a-z]\w*(?=:)/i,
    lookbehind: true,
    alias: 'property',
  },
  access: {
    pattern: /(\.\s*)[$_a-z][\w$]*/i,
    lookbehind: true,
    alias: 'property',
  },
  class: {
    pattern: /\b[A-Z]\w+\b/,
    alias: 'class-name',
  },
});

Prism.languages.insertBefore('javascript', 'function', {
  method: {
    pattern: /(\.\s*)[$_a-z][\w$]*(?=(\())/i,
    lookbehind: true,
    alias: 'function',
  }
});
