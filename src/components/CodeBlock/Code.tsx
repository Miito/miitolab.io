import React from 'react';
import * as S from './styles';
import Highlight, { defaultProps, Language } from 'prism-react-renderer';
import { purple_2, purple_text_2 } from '@/styles/colors';
require('./lang/javascript');

const customTheme = {
  plain: {
    backgroundColor: purple_2,
    color: purple_text_2
  },
  styles: [
  ]
};

interface CodeProps {
  children?: string;
  className?: string;
}

const Code: React.FC<CodeProps> = ({ children, className }) => {
  const language = className ? className.replace(/language-/, '') as Language: 'js' as Language;

  return (
    <Highlight {...defaultProps} code={children} language={language} theme={ customTheme } >
      {({ className, style, tokens, getLineProps, getTokenProps }) => (
        <S.Wrapper className={className} style={{ ...style }}>
          <S.Code>
            {tokens.map((line, i) => {
              if (i === tokens.length - 1 && line[0].empty) return false;
              return (
                <S.Line key={i} {...getLineProps({ line, key: i })}>
                  <S.LineNo>{i + 1}</S.LineNo>
                  <S.LineContent>
                    {line.map((token, key) => (
                      <span key={key} {...getTokenProps({ token, key })} />
                    ))}
                  </S.LineContent>
                </S.Line>
              );
            })}
          </S.Code>
        </S.Wrapper>
      )}
    </Highlight>
  );
};

export default Code;
