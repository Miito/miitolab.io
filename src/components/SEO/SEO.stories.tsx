import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import SEO, { SEOProps } from './SEO';

export default {
  title: 'SEO',
  component: SEO,
} as Meta;

const Template: Story<SEOProps> = (args) => (
  <SEO {...args} />
);

export const SEOStory = Template.bind({});

SEOStory.args = {};
