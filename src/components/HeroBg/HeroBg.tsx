/* eslint-disable unicorn/no-null */
import React, { Suspense, useEffect, useRef, useState } from 'react';
import { Canvas, useFrame, useThree } from 'react-three-fiber';
import { Quaternion, Euler,  } from 'three';
import * as S from './styles';
import * as THREE from 'three';
import DotGothic16 from 'assets/fonts/DotGothic16/DotGothic16_Regular.json';
import useMousePosition from '@/utils/hook/useMousePosition';
import { animate } from 'framer-motion';
import useWindowSize from '@/utils/hook/useWindowSize';
import useDeviceOrientation from '@/utils/hook/useDeviceOrientation';

function lerp(v0, v1, t) {
  return v0 * (1-t) + v1 * t;
}

function TextMesh({ mouse, hover }: { mouse: [number, number], hover: boolean }) {
  const ref = useRef();
  const text = '(´･ω･`)';
  const { aspect } = useThree();
  const [rotationZ, setRotationZ] = useState(0);

  useEffect(() => {
    const controls = animate(0, [0.2, 0, 0.2], {
      duration: 1,
      repeat: Number.POSITIVE_INFINITY,
      repeatType: 'mirror',
      type: 'spring',

      onUpdate(value) {
        setRotationZ(value);
      },
    });

    return controls.stop;
  }, []);

  useFrame(() => {
    if (ref.current) {
      ref.current.rotation.x = lerp(ref.current.position.x, mouse.current[1] / aspect / 50, 0.1);
      ref.current.rotation.y = lerp(ref.current.position.y, mouse.current[0] / aspect / 50, 0.1);
      ref.current.geometry.center();
    }

    if (hover) {
      ref.current.rotation.z = rotationZ;
    } else {
      ref.current.rotation.z = 0;
    }
  });

  const font = new THREE.FontLoader().parse(DotGothic16);

  const textOptions = {
    font,
    size: 3,
    height: 1
  };

  return (
    <Suspense fallback={null}>
      <mesh position={[0, 0, -10]} ref={ref}>
        <textGeometry
          attach='geometry'
          args={[text, textOptions]}
        >
        </textGeometry>
        <meshBasicMaterial attach='material'/>
      </mesh>
    </Suspense>
  );
}

const HeroBg: React.FC = () => {
  const [hover, setHover] = useState(false);
  const mouse = useRef([0, 0]);
  const { x, y } = useMousePosition();

  useEffect(() => {
      mouse.current = [
        x - window.innerWidth / 2,
        y - window.innerHeight / 2
      ];
  }, [x, y]);

  const links = typeof window !== 'undefined' ? document.querySelectorAll('a') : undefined;

  useEffect(() => {
    const handleHoverIn = () => {
      setHover(true);
    };

    const handleHoverOut = () => {
      setHover(false);
    };

    for (const link of links) {
      link.addEventListener('mouseover', handleHoverIn, { passive: true });
      link.addEventListener('mouseout', handleHoverOut, { passive: true });
    }
    return () => {
      for (const link of links) {
        link.removeEventListener('mouseover', handleHoverIn);
        link.removeEventListener('mouseout', handleHoverOut);
      }
    };
  }, [hover, links]);

  return (
    <S.HeroBg>
      <Canvas
        camera={{ position: [0, 0, 0] }}
      >
        <TextMesh mouse={mouse} hover={hover}/>
      </Canvas>
    </S.HeroBg>
  );
};

export default HeroBg;
