import { Breakpoints } from '@/styles/breakpoint';
import styled from 'styled-components';

export const HeroBg = styled.div`
  position: absolute;
  width: 100%;
  height: 150%;
  top: 0;
  right: 0;

  @media (max-width: ${Breakpoints.medium}) {
    height: 100%;
  }
`;
