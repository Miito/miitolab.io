import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import HeroBg, { HeroBgProps } from './HeroBg';

export default {
  title: 'HeroBg',
  component: HeroBg,
} as Meta;

const Template: Story<HeroBgProps> = (args) => (
  <HeroBg {...args} />
);

export const HeroBgStory = Template.bind({});

HeroBgStory.args = {};
