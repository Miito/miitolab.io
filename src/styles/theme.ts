import { fontFamily, wordmarkFontFamily, monospaceFontFamily } from './typography';
import * as colors from './colors';
import * as config from './config';

export type ThemeType = typeof theme;

export const theme = {
  colors: {
    ...colors
  },
  types: {
    fontFamily,
    wordmarkFontFamily,
    monospaceFontFamily,
  },
  ...config
};
