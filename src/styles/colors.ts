// Main
export const background = '#0E0421';

export const mito_purple_1 = '#3B00A8';
export const mito_purple_2 = '#5F19DD';

// Text
export const purple_text_1 = '#F3ECFF';
export const purple_text_2 = '#D7C9F0';

// Purple
export const purple_1 = '#171224';
export const purple_2 = '#231C33';
export const purple_3 = '#352A4D';
export const purple_4 = '#594780';
export const purple_5 = '#745DA6';


// Code

export const code_purple_1 = '#b38aff';
