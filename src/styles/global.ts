import { createGlobalStyle, css } from 'styled-components';
import { ThemeType } from 'styles/theme';
export const GlobalStyle = createGlobalStyle(({ theme }: { theme: ThemeType }) => css`
  *,
  ::before,
  ::after {
    box-sizing: inherit;
  }

  html {
    background-color: ${theme.colors.purple_2};
    box-sizing: border-box;
    margin: 0;
    max-width: 100vw;
    padding: 0;
  }

  body {
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    margin: 0;
    padding: 0;
    max-width: 100vw;
    background-color: ${theme.colors.background};
    color: ${theme.colors.purple_text_1};
  }

  div {
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
  }

  code {
    background: ${theme.colors.purple_2};
    padding: 2px 5px;
    border-radius: 6px;
  }

  ::selection {
    background-color: ${theme.colors.mito_purple_2};
    color: ${theme.colors.purple_text_1};
    text-shadow: none;
  }

  #___gatsby,
  #gatsby-focus-wrapper {
    position: relative;
    min-height: 100%;
  }

  #gatsby-focus-wrapper {
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: stretch;
  }
`);
