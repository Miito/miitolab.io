import * as React from 'react';
import MainLayout from 'layouts/Main';
import { MaxWidth, MDXRenderer, SEO } from 'components';
import { MDXProvider } from '@mdx-js/react';

import AboutContent from 'content/about.mdx';

const About: React.FC = () => {
  return (
    <MainLayout>
      <SEO title="about"/>
      <MaxWidth>
        <MDXProvider components={MDXRenderer} >
          <AboutContent />
        </MDXProvider>
      </MaxWidth>
    </MainLayout>
  );
};

export default About;
