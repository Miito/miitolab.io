import * as React from 'react';
import MainLayout from 'layouts/Main';
import { Hero, SEO } from 'components';

const IndexPage: React.FC = () => {
  return (
    <MainLayout>
      <SEO title="home"/>
      <Hero />
    </MainLayout>
  );
};

export default IndexPage;
