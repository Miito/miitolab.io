import { graphql } from 'gatsby';
import PortfolioRenderer from '@/components/Renderer/Portfolio/PortfolioRenderer';

export const pageQuery = graphql`
query PortfoliosIndex {
  allMdx(filter: {fileAbsolutePath: {glob: "**/portfolios/**"}}, sort: {fields: [frontmatter___date], order: DESC}) {
    edges {
      node {
        id
        excerpt
        slug
        body
        frontmatter {
          title
          date
          tags
          description
          image {
            childImageSharp {
              gatsbyImageData(
                layout: CONSTRAINED
                placeholder: BLURRED
                formats: [AUTO, WEBP, AVIF]
              )
            }
          }
          imageAlt
        }
      }
    }
  }
}
`;

export default PortfolioRenderer;
