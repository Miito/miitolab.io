import { graphql } from 'gatsby';
import DownloadRenderer from '@/components/Renderer/Download/DownloadRenderer';

export const pageQuery = graphql`
query DownloadsIndex {
  allMdx(filter: {fileAbsolutePath: {glob: "**/downloads/**"}}, sort: {fields: [frontmatter___date], order: DESC}) {
    edges {
      node {
        id
        excerpt
        slug
        body
        frontmatter {
          title
          date
          tags
          description
          image {
            childImageSharp {
              gatsbyImageData(
                layout: CONSTRAINED
                placeholder: BLURRED
                formats: [AUTO, WEBP, AVIF]
              )
            }
          }
          imageAlt
        }
      }
    }
  }
}
`;

export default DownloadRenderer;
