import * as React from 'react';
import MainLayout from 'layouts/Main';
import { Button, MaxWidth, SEO } from 'components';
import { Link } from 'gatsby';
import styled from 'styled-components';

const noStyleLink = styled(Link)`
  &::before {
    display: none;
  }
`;

const NotFoundPage: React.FC = () => {
  return (
    <MainLayout>
      <SEO title="404"/>
      <MaxWidth>
        <h1>NOT FOUND</h1>
        <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
        <Button as={ noStyleLink } to="/">Go Home</Button>
      </MaxWidth>
    </MainLayout>
  );
};

export default NotFoundPage;
