import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import Img, { ImgProps } from './Img';

export default {
  title: 'Img',
  component: Img,
} as Meta;

const Template: Story<ImgProps> = (args) => (
  <Img {...args} />
);

export const ImgStory = Template.bind({});

ImgStory.args = {};
