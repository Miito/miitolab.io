import { Breakpoints } from '@/styles/breakpoint';
import styled, { css } from 'styled-components';
import { AnchorType } from './Img';

const CustomAnchor = (props) => css`
  ${(props: { anchor: AnchorType }) => {
    switch (props.anchor) {
      case 'none':
        break;
      case 'left':
        return `
          float: left;
          margin-right: 20px;
          margin-left: 0;

        `;
      case 'right':
        return `
          float: right;
          margin-left: 20px;
          margin-right: 0;
        `;
      case 'center':
      default:
        return `
          display: block;
          margin-left: auto;
          margin-right: auto;
        `;
    }
  }}
`;

export const Img = styled.img`
  width: 100%;
  ${(props: { anchor: AnchorType }) => {
    if (props.anchor === 'center') {
      return css`
        width: 100%;
        max-width: 60%;
        display: block;
        margin: 0 auto;
      `;
    }
  }}
`;

export const Figure = styled.figure`
  ${(props: { anchor: AnchorType }) => CustomAnchor(props.anchor)}
`;

export const Figcaption = styled.figcaption`
  text-align: center;
`;
