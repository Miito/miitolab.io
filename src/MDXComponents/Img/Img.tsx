import React from 'react';
import * as S from './styles';

export type AnchorType = 'left' | 'center' | 'right' | 'none';

export interface ImgProps {
  figcaption: string;
  anchor: AnchorType;
  src: string;
  props?: unknown;
}

const Img: React.FC<ImgProps> = ({ figcaption, anchor, src, props }) => {
  return (
    <>
      {figcaption
        ? <S.Figure anchor={ anchor }>
            <S.Img anchor={ anchor } src={src} {...props} />
            <S.Figcaption>{figcaption}</S.Figcaption>
          </S.Figure>
        : <S.Img anchor={anchor} src={src} {...props} />
      }
    </>
  );
};

export default Img;
