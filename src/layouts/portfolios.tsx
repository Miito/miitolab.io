import React from 'react';
import MainLayout from './Main';
import { MaxWidth, Post, SEO } from 'components';
import styled from 'styled-components';
import { graphql } from 'gatsby';

const Content = styled.article`
  margin-top: 20px;
  margin-bottom: 106px;
  display: flex;
  flex-direction: column;
`;

export const pageQuery = graphql`
  query PortfolioQuery($id: String) {
    mdx(id: { eq: $id }) {
      id
      body
      frontmatter {
        title
        description
        date
        tags
        ogImage
      }
    }
  }
`;

interface PostProps {
  data: GatsbyTypes.PortfolioQueryQuery;
  children?: React.ReactNode;
}

const PostLayout: React.FC<PostProps> = ({ data: { mdx } }) => {
  return (
    <MainLayout>
      <SEO title="post"/>
      <Content>
        <MaxWidth>
          <Post {...mdx} header="h1"/>
        </MaxWidth>
      </Content>
    </MainLayout>
  );
};

export default PostLayout;
