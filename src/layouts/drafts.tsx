import { SEO } from 'components';
import React from 'react';
import MainLayout from './Main';

interface PostProps {
  children?: React.ReactNode;
}

const DraftLayout: React.FC<PostProps> = ({ children }) => {
  return (
    <MainLayout>
      <SEO title="drafts"/>
      { children }
    </MainLayout>
  );
};

export default DraftLayout;
