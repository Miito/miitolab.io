import React from 'react';
import MainLayout from './Main';

interface PostProps {
  children?: React.ReactNode;
}

const DownloadLayout: React.FC<PostProps> = ({ children }) => {
  return (
    <MainLayout>
      { children }
    </MainLayout>
  );
};

export default DownloadLayout;
