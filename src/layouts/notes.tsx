import { SEO } from 'components';
import React from 'react';
import MainLayout from './Main';

interface PostProps {
  children?: React.ReactNode;
}

const NoteLayout: React.FC<PostProps> = ({ children }) => {
  return (
    <MainLayout>
      <SEO title="notes"/>
      { children }
    </MainLayout>
  );
};

export default NoteLayout;
