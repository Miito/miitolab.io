import React from 'react';
import MainLayout from './Main';
import { SEO } from 'components';

interface HomeProps {
  children?: React.ReactNode;
}

const HomeLayout: React.FC<HomeProps> = ({ children }) => {
  return (
    <MainLayout>
      <SEO title="home"/>
      { children }
    </MainLayout>
  );
};

export default HomeLayout;
