import { GitHub, GitLab } from 'assets/icons/';

interface SocialMediaType {
  [key: string]: {
    title: string;
    url: string;
    fill: string;
    hover: string;
    component: (className: unknown, props: unknown) => JSX.Element;
  }
}

const socialMedia: SocialMediaType = {
  github: {
    title: 'GitHub',
    url: 'https://github.com/MitoKito',
    fill: '#fff',
    hover: '#bbc6cc',
    component: GitHub,
  },
  gitlab: {
    title: 'GitLab',
    url: 'https://gitlab.com/Miito',
    fill: '#fff',
    hover: '#FCA121',
    component: GitLab,
  },
};

export default socialMedia;
