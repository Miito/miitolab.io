import { Code } from './Code';
import { Paint } from './Paint';
import { Search } from './SearchIcon';

export default {
  Code,
  Paint,
  Search
};

export { ReactComponent as Express } from './express.svg';
export { ReactComponent as GitHub } from './github.svg';
export { ReactComponent as GitLab } from './gitlab.svg';
export { ReactComponent as HTML5 } from './html5.svg';
export { ReactComponent as JavaScript } from './javascript.svg';
export { ReactComponent as jQuery } from './jquery.svg';
export { ReactComponent as MySql } from './mysql.svg';
export { ReactComponent as NodeJS } from './node-dot-js.svg';
export { ReactComponent as Passport } from './passport.svg';
export { ReactComponent as ReactIcon } from './react.svg';
export { ReactComponent as Sequelize } from './sequelize.svg';
