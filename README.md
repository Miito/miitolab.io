<p align="center">
  <img alt="Brian Logo" src="src/images/icon.svg" width="60" />
</p>
<h1 align="center">
  Brian Dinh's website
</h1>

[https://miito.dev](https://miito.dev)

## Usage
```shell
$ git clone https://github.com/MitoKito/mito-blog.git
$ cd mito-blog
$ npm install
$ npm develop
```

## Generate code
To generate code run:
```shell
$ npm run c:component NAME
$ npm run c:mdxcomponent NAME
$ npm run c:download NAME
$ npm run c:draft NAME
$ npm run c:note NAME
$ npm run c:post NAME
```