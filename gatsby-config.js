module.exports = {
  siteMetadata: {
    title: 'mito blog',
    author: 'Brian Dinh',
    description: 'blog and stuff',
    siteUrl: 'https://www.miito.dev',
    navLinks: [
      {
        name: 'home',
        link: '/'
      },
      {
        name: 'about',
        link: '/about'
      },
      {
        name: 'post',
        link: '/posts',
      },
      {
        name: 'portfolio',
        link: '/portfolios',
      },
      {
        name: 'download',
        link: '/downloads',
      },
    ]
  },
  plugins: [
    'gatsby-plugin-eslint',
    'gatsby-plugin-styled-components',
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: {
        trackingId: 'UA-151892173-1',
      },
    },
    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        prettier: true,
        svgo: true,
        svgoConfig: {
          plugins: [
            { removeViewBox: true },
            { cleanupIDs: true },
            { removeDimensions: true },
            { removeUselessStrokeAndFill: true },
          ],
        },
      },
    },
    'gatsby-plugin-sharp',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sitemap',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'mito blog',
        short_name: 'mito blog',
        icon: 'src/images/icon.png',
        cache_busting_mode: 'none',
        display: 'standalone',
        start_url: '/',
        theme_color: '#231C33',
        background_color: '#0E0421'
      },
    },
    {
      resolve: 'gatsby-plugin-mdx',
      options: {
        extensions: ['.mdx', '.md'],
        gatsbyRemarkPlugins: [
          {
            resolve: 'gatsby-remark-images',
          },
        ],
      },
    },
    'gatsby-plugin-image',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: './src/images/',
      },
      __key: 'images',
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'pages',
        path: './src/pages/',
      },
      __key: 'pages',
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'downloads',
        path: './src/content/downloads',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'drafts',
        path: './src/content/drafts',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'notes',
        path: './src/content/notes',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'posts',
        path: './src/content/posts',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'portfolios',
        path: './src/content/portfolios',
      },
    },
    {
      resolve: 'gatsby-plugin-webpack-bundle-analyser-v2',
      options: {
        analyzerMode: 'server',
        analyzerPort: '8888',
        devMode: true,
      },
    },
    'gatsby-plugin-catch-links',
    {
      resolve: 'gatsby-plugin-typegen',
      options: {
        emitSchema: {
          'src/__generated__/gatsby-introspection.json': true,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-offline',
      options: {
        precachePages: ['/', '/about/', '/posts/*', '/portfolios/']
      }
    }
  ],
};
